# Configurations

## `GET /v2/configurations`

Scope: `global_read`

Retrieve stored configurations for all sites.

Supports the `if-modified-since` header to return a `304` status code if no configurations have been changed.

```js
[
  {
    domain: 'example.net',
    modified: '2020-02-20T02:20:02.202',
    configuration: { ... }
  },
  {
    domain: 'example.com',
    modified: '2020-10-10T10:10:10.101',
    configuration: { ... }
  }
]
```

## `GET /v2/sites{/domain}/configuration`

Scope: `global_read` or `deploy`

Retrieve the stored configuration for a domain.

```js
{
  modified: '2020-02-20T02:20:02.202',
  domain: 'example.net',
  configuration: {
    // ... host options ...
  }
}
```

## `PATCH /v2/sites{/domain}/configuration`

*Not yet implemented.*

Edit the site configuration.

## `PUT /v2/sites{/domain}/configuration`

*Not yet implemented.*

Set the site configuration.
