# Deployments

## `DELETE /v2/sites{/domain}`

Scope: `deploy`

Unpublish a site. This removes all configuration data and files associated with the site.

Returns `200` upon success, with an empty body.

## `PUT /v2/sites{/domain}`

Scope: `deploy`

Deploys a site by the given domain. Optionally provide files and a configuration.

If the `domain` value is invalid, a `400` status is returned. If the site is owned by another user, a `403` status is returned.

Content type of the request must be `multipart/form-data`.

The `configuration` field contains JSON data of the host options. A `400` status is returned if the data does not comply with the [@commonshost/configuration](https://gitlab.com/commonshost/configuration) `host` JSON Schema.

There are constraints on the number of individual files as well as the total size of all files.

One or more `directory` fields contain all files to be deployed. The filename may contain a relative path, as per the [`webkitdirectory` attribute](https://wicg.github.io/entries-api/#dom-htmlinputelement-webkitdirectory). Any previously hosted files for the domain are removed. To update or remove only a single file, see the `/v2/sites{/domain}/files{/file}` endpoint.

If a new certificate is required, it will be issued asynchronously.

The response message is also published as a notification.

Returns `200` upon success with an object containing the published domain.

```js
{
  type: 'site-deploy',
  domain: 'example.net',
  isNewDomain: true,
  hasNewFiles: true,
  hasNewConfiguration: true
}
```

## `GET /v2/sites`

List all sites owned by the authenticated user.

```js
[
  {
    modified: '2020-02-20T02:20:02.202',
    userId: 'auth0|1234567890',
    domain: 'example.net'
  },
  {
    modified: '2020-10-10T10:10:10.101',
    userId: 'auth0|abcdef',
    domain: 'example.com'
  }
]
```
