# Deprecated

## `POST /v1/deploy`

*Deprecated*

Use `PUT /v2/sites{/domain}` instead.

## `POST /v1/sites`

*Deprecated*

Use `PUT /v2/sites{/domain}` instead.
