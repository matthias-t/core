# Domains

## `GET /v2/domains/available{?domain}`

Check whether a domain is still available or already taken by a site.

Returns `200` status code if the domain is available, otherwise returns `410` status code. The response body is always empty.

## `GET /v2/domains/suggestions{?project}`

Retrieve a list of suggested and available domains. Up to 5 strings are returned.

Domains are based on the given project name or randomly generated.

```js
[
  'project.example.net',
  'finest-witty-turtle.example.net',
  'magnificent-kind-sloth.example.net'
]
```
