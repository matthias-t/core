# Endpoints

## Authentication

Supply a JSON Web Token (JWT) to authenticate the request. The token is issued by Auth0 and signed using the `RS256` algorithm.

```
Authorization: Bearer eyJhbGciOiJSUzI1NiI...
```

## Content Type

Requests must include a valid content type header. Typically indicating JSON or a multipart request.

```
curl https://api.commons.host:8888/v1/sites \
  -H "content-type: application/json" \
  ; echo
```

## Errors

A response with HTTP status code 4xx-5xx contains JSON with the following properties.

- `message` A human readable description of the problem.
- `statusCode` The HTTP status code.
- `error` The HTTP status message.

Example:

```json
{
  "message": "Missing uploaded files",
  "error": "Internal Server Error",
  "statusCode": 500
}
```

## IDN

Any use of domain names, in URL path segments or other instances, supports the use of internationalised domain names (IDN). Values are automatically converted to lowercase and normalised to Punycode using the [`ToASCII` algorithm](https://tools.ietf.org/html/rfc3490#section-4.1). For example `💩.EXAMPLE.NET` becomes `xn--ls8h.example.net`.
