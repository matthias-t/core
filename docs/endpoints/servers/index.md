# Servers

## `PUT /v2/servers/{id}/ddns`

Scope: `ddns`

Dynamic DNS for edge servers to update the DNS type A record.

Requires [client credentials](https://auth0.com/docs/api-auth/tutorials/client-credentials), i.e. client ID and client secret. Only available for machine-to-machine applications.

Request body must include the public IPv4 address of the server.

Body:

```js
{
  ipv4: '10.20.30.40'
}
```

Responds with `200` status code and the `ipv4` address.

Response:

```js
{
  ipv4: '10.20.30.40'
}
```
