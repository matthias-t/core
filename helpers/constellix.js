const crypto = require('crypto')
const fetch = require('node-fetch')

function createSecurityToken (apiKey, secretKey) {
  const requestDate = String(Date.now())
  const hmac = crypto.createHmac('sha1', secretKey)
  hmac.update(requestDate)
  const securityToken = `${apiKey}:${hmac.digest('base64')}:${requestDate}`
  return securityToken
}

async function constellix (endpoint, {
  apiKey,
  secretKey,
  origin,
  version = 'v1',
  ...fetchOptions
}) {
  const url = `${origin}/${version}/${endpoint}`
  const securityToken = createSecurityToken(apiKey, secretKey)
  const headers = {
    'Content-Type': 'application/json',
    'x-cns-security-token': securityToken
  }
  const response = fetch(url, {
    ...fetchOptions,
    headers
  })
  return response
}

module.exports.constellix = constellix
