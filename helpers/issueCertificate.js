const shellEscape = require('shell-escape')
const { exec } = require('child-process-promise')
const { join } = require('path')
const { promises: { readFile, rmdir } } = require('fs')
const ms = require('ms')

async function issueCertificate (fastify, domain) {
  const { configuration, mongo: { db }, pubnub } = fastify

  if (configuration.acme.wildcards.some((suffix) => domain.endsWith(suffix))) {
    throw new Error(`Not renewing wildcard certificate for: ${domain}`)
  }

  const domains = [domain]

  const certificate = await db.collection('certificates').findOne({ domain })

  if (certificate !== null) {
    if (certificate.modified instanceof Date) {
      const modifiedAt = certificate.modified.getTime()
      if (!Number.isNaN(modifiedAt)) {
        const timeSinceIssued = Date.now() - modifiedAt
        if (timeSinceIssued < ms('30 days')) {
          throw new Error(`Not renewing fresh certificate for: ${domain}`)
        }
      }
    }

    if (Array.isArray(certificate.san)) {
      const alternates = certificate.san
        .filter((alternate) => alternate !== domain)
      const uniques = new Set(alternates)
      domains.push(...uniques)
    }
  }

  fastify.log.info(`Renewing certificate for: ${domain}`)

  const directory = join(configuration.acme.home, `${domain}_ecc`)

  try {
    await exec(shellEscape([
      configuration.acme.acmesh,
      '--issue',
      '--force',
      '--keylength', 'ec-256',
      '--webroot', configuration.acme.webroot,
      '--home', configuration.acme.home,
      ...domains.reduce((args, name) => [...args, '--domain', name], [])
    ]), { capture: ['stdout', 'stderr'] })
  } catch (error) {
    await rmdir(directory, { recursive: true })
    if (!error.stderr.includes('Domain key exists')) {
      throw error
    }
  }

  const [key, cert] = await Promise.all([
    readFile(join(directory, `${domain}.key`)),
    readFile(join(directory, 'fullchain.cer'))
  ])

  await rmdir(directory, { recursive: true })

  await db.collection('certificates').updateOne(
    { domain },
    {
      $set: { domain, san: domains.slice(1), ca: [], key, cert },
      $currentDate: { modified: true }
    },
    { upsert: true }
  )

  pubnub.publish({
    channel: configuration.pubnub.channels[0],
    message: { type: 'certificate-issue', domain }
  })
}

module.exports.issueCertificate = issueCertificate
