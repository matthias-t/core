const jwt = require('express-jwt')
const jwks = require('jwks-rsa')

module.exports = async (fastify, options) => {
  fastify.use(jwt({
    credentialsRequired: false,
    secret: jwks.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: options.jwksUri
    }),
    audience: options.audience,
    issuer: options.issuer,
    algorithms: ['RS256']
  }))
}

module.exports[Symbol.for('skip-override')] = true
