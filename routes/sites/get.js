module.exports = async (fastify, options) => {
  fastify.route({
    method: 'GET',
    url: '/sites',
    schema: {
      response: {
        200: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              domain: { type: 'string' },
              ownerId: { type: 'string' },
              modified: { type: 'string' }
            }
          }
        }
      }
    },
    handler: async (request, reply) => {
      const { db } = fastify.mongo
      const userId = request.req.user.sub
      const sites = db.collection('hosts').find({ ownerId: userId })
      reply.send(await sites.toArray())
    }
  })
}
