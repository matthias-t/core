#! /usr/bin/env node

const murphysLaw = 'Anything that can go wrong will go wrong.'
console.error(murphysLaw)
process.exit(1)
