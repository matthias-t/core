const test = require('blue-tape')
const { fetch } = require('./helpers/fetch')
const configuration = require('../core.conf.example.js')
const { getDdnsAccessToken } = require('./helpers/credentials')
const { createServer } = require('http')
const {
  constants: {
    HTTP_STATUS_BAD_REQUEST,
    HTTP_STATUS_FORBIDDEN,
    HTTP_STATUS_OK
  }
} = require('http2')

const mongo = require('./helpers/database')(configuration)
const server = require('..')

let constellix
test('Mock Constellix API', async (t) => {
  constellix = createServer(async (request, response) => {
    const { method, url } = request
    console.log(method, url)
    if (method === 'GET' && url === '/v1/domains') {
      const body = [
        { id: 123, name: 'example.com' }
      ]
      response.end(JSON.stringify(body))
    } else if (method === 'GET' && url === '/v1/domains/123/records/A') {
      const body = [
        { id: 1, name: 'aa-zzz-1' },
        { id: 2, name: 'aa-zzz-2' },
        { id: 3, name: 'aa-zzz-3' }
      ]
      response.end(JSON.stringify(body))
    } else if (method === 'PUT' && url === '/v1/domains/123/records/A/1') {
      const body = []
      for await (const chunk of request) {
        body.push(chunk)
      }
      const raw = Buffer.concat(body)
      const data = JSON.parse(raw)
      t.deepEqual(data, {
        recordOption: 'roundRobin',
        name: 'aa-zzz-1',
        ttl: 60,
        roundRobin: [{
          disableFlag: false,
          value: '10.20.30.40'
        }]
      })
      response.end()
    } else {
      response.end()
      t.fail('Unknown endpoint')
    }
  })
  constellix.listen(0)
})

let fastify
test('Start server', async (t) => {
  const { port } = constellix.address()
  configuration.constellix.origin = `http://localhost:${port}`
  configuration.constellix.domain = 'example.com'
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

test('Setup database fixtures', async (t) => {
  await mongo.db.collection('servers').deleteMany()
  await mongo.db.collection('servers').insertMany([
    {
      // IP will change to 10.20.30.40
      serverId: 'aa-zzz-1',
      modified: new Date(),
      allowDdns: true,
      ipv4: '10.10.10.10'
    },
    {
      // DDNS disabled
      serverId: 'aa-zzz-2',
      modified: new Date(),
      allowDdns: false
    },
    {
      // Same IP already set
      serverId: 'aa-zzz-3',
      modified: new Date(),
      allowDdns: true,
      ipv4: '10.20.30.40'
    }
  ])
})

test('Update DDNS', async (t) => {
  const { port } = fastify.server.address()
  const origin = `https://localhost:${port}`
  const url = `${origin}/v2/servers/aa-zzz-1/ddns`
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'content-type': 'application/json',
      authorization: `Bearer ${await getDdnsAccessToken()}`
    },
    body: JSON.stringify({ ipv4: '10.20.30.40' })
  })
  t.is(response.status, HTTP_STATUS_OK)
  const body = await response.json()
  t.deepEqual(body, { ipv4: '10.20.30.40' })
})

test('Reject if DDNS is not enabled', async (t) => {
  const { port } = fastify.server.address()
  const origin = `https://localhost:${port}`
  const url = `${origin}/v2/servers/aa-zzz-2/ddns`
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'content-type': 'application/json',
      authorization: `Bearer ${await getDdnsAccessToken()}`
    },
    body: JSON.stringify({ ipv4: '10.20.30.40' })
  })
  t.is(response.status, HTTP_STATUS_FORBIDDEN)
})

test('Idempotent if same IP is already set', async (t) => {
  const { port } = fastify.server.address()
  const origin = `https://localhost:${port}`
  const url = `${origin}/v2/servers/aa-zzz-3/ddns`
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'content-type': 'application/json',
      authorization: `Bearer ${await getDdnsAccessToken()}`
    },
    body: JSON.stringify({ ipv4: '10.20.30.40' })
  })
  t.is(response.status, HTTP_STATUS_OK)
  const body = await response.json()
  t.deepEqual(body, { ipv4: '10.20.30.40' })
})

test('Reject if not a valid server ID', async (t) => {
  const { port } = fastify.server.address()
  const origin = `https://localhost:${port}`
  const url = `${origin}/v2/servers/foo-bar-baz/ddns`
  const response = await fetch(url, {
    method: 'PUT',
    headers: {
      'content-type': 'application/json',
      authorization: `Bearer ${await getDdnsAccessToken()}`
    },
    body: JSON.stringify({ ipv4: '10.20.30.40' })
  })
  t.is(response.status, HTTP_STATUS_BAD_REQUEST)
})

test('Stop server', (t) => fastify.close())
test('Stop mock Constellix', (t) => constellix.close(t.end))
