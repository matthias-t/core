const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const { s3cmd } = require('../helpers/s3cmd')
const { deploy } = require('./helpers/deploy')
const { getUserAccessToken, getUserId } = require('./helpers/credentials')
const PubNub = require('pubnub')
const { join } = require('path')
const { chmod, rmdir } = require('fs').promises
const ms = require('ms')
const { sleep } = require('./helpers/sleep')
const {
  constants: {
    HTTP_STATUS_OK,
    HTTP_STATUS_UNAUTHORIZED
  }
} = require('http2')

const original = {
  acmesh: configuration.acme.acmesh,
  s3cmd: configuration.s3.s3cmd
}

const mongo = require('./helpers/database')(configuration)
const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

const fixture = {}

test('Setup database fixtures', async (t) => {
  fixture.userId = await getUserId()
  fixture.modified = new Date()
  fixture.domain = 'fixture.example.net'

  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('configurations').deleteMany()
  await mongo.db.collection('certificates').deleteMany()
})

test('Reset & mock acme.sh', async (t) => {
  const acmejs = join(__dirname, 'helpers/acme.js')
  configuration.acme.acmesh = acmejs
  await chmod(acmejs, '755')
  await rmdir(configuration.acme.home, { recursive: true })
})

test('Resetting object storage', async (t) => {
  return s3cmd(
    configuration.s3,
    '--recursive',
    '--force',
    'del',
    `s3://${configuration.s3.bucket}`
  )
})

test('Setup client fixtures', async (t) => {
  fixture.files = [
    {
      data: 'hello world',
      filepath: 'index.html'
    },
    {
      data: 'fake code',
      filepath: 'scripts/app.js'
    },
    {
      data: 'dummy',
      filepath: 'unknown_mime_type'
    }
  ]

  fixture.accessToken = await getUserAccessToken()
  fixture.port = configuration.port
})

test('Deploy a site', async (t) => {
  const expectedDeploy = {
    type: 'site-deploy',
    domain: fixture.domain,
    isNewDomain: true,
    hasNewFiles: true,
    hasNewConfiguration: true
  }

  const subscribeKey = configuration.pubnub.subscribeKey
  const pubnub = new PubNub({ subscribeKey })
  pubnub.subscribe({ channels: configuration.pubnub.channels })
  const pubnubNotification = new Promise((resolve) => {
    const expectedMessageCount = 2
    let receivedMessageCount = 0
    pubnub.addListener({
      status (status) {
        console.log(status)
      },
      message ({ message }) {
        console.log('PubNub:', message)
        if (message.type === 'site-deploy') {
          t.deepEqual(message, expectedDeploy)
          receivedMessageCount++
        }
        if (message.type === 'certificate-issue') {
          const expectedIssue = {
            type: 'certificate-issue',
            domain: fixture.domain
          }
          t.deepEqual(message, expectedIssue)
          receivedMessageCount++
        }
        if (receivedMessageCount === expectedMessageCount) {
          pubnub.stop()
          resolve()
        }
      }
    })
  })

  const response = await deploy(fixture)

  t.true(response.ok)
  t.deepEqual(await response.json(), expectedDeploy)

  await sleep(10000)

  const hostOptions = await mongo.db.collection('configurations')
    .findOne({ domain: fixture.domain })
  t.not(hostOptions, null)
  t.is(hostOptions.domain, fixture.domain)
  t.true('configuration' in hostOptions)
  t.true('modified' in hostOptions)

  const listPublic = await s3cmd(
    configuration.s3,
    'ls',
    `s3://${configuration.s3.bucket}/sites/${fixture.domain}/public/`
  )

  t.ok(/public\/index\.html/.test(listPublic.stdout))
  t.ok(/public\/index\.html\.br/.test(listPublic.stdout))
  t.notOk(/public\/index\.html\.gz/.test(listPublic.stdout))

  const listCrypto = await s3cmd(
    configuration.s3,
    'ls',
    `s3://${configuration.s3.bucket}/sites/${fixture.domain}/crypto/`
  )

  t.notOk(/crypto\/key.pem/.test(listCrypto.stdout))
  t.notOk(/crypto\/cert.pem/.test(listCrypto.stdout))

  await pubnubNotification

  const actualCertificate = await mongo.db.collection('certificates')
    .findOne({ domain: fixture.domain })
  t.not(actualCertificate, null)
  t.is(actualCertificate.domain, fixture.domain)
  t.deepEqual(actualCertificate.san, [])
  t.deepEqual(actualCertificate.ca, [])
  t.deepEqual(actualCertificate.key, Buffer.from('private key'))
  t.deepEqual(actualCertificate.cert, Buffer.from('public certificate chain'))
  t.not(Number.isNaN(new Date(actualCertificate.modified).getTime()))
})

test('Re-deploying too soon does not renew certificate', async (t) => {
  const expectedCertificate = await mongo.db.collection('certificates')
    .findOne({ domain: fixture.domain })
  await deploy(fixture)
  await sleep(10000)
  const actualCertificate = await mongo.db.collection('certificates')
    .findOne({ domain: fixture.domain })
  t.equal(
    actualCertificate.modified.getTime(),
    expectedCertificate.modified.getTime()
  )
})

test('Re-deploying after some time renews certificate', async (t) => {
  const expired = new Date(Date.now() - ms('60 days'))
  await mongo.db.collection('certificates').updateOne(
    { domain: fixture.domain },
    { $set: { modified: expired } }
  )
  await deploy(fixture)
  await sleep(10000)
  const actualCertificate = await mongo.db.collection('certificates')
    .findOne({ domain: fixture.domain })
  t.notEqual(
    actualCertificate.modified.getTime(),
    expired.getTime()
  )
})

test('Mock s3cmd with always-failing implementation', async (t) => {
  const s3cmdjs = join(__dirname, 'helpers/s3cmd.js')
  configuration.s3.s3cmd = s3cmdjs
  await chmod(s3cmdjs, '755')
})

test('Clean up temporary files on failure to upload', async (t) => {
  const response = await deploy(fixture)
  t.true(response.ok)
  await sleep(5000)
})

test('Restore s3cmd', async (t) => {
  configuration.s3.s3cmd = original.s3cmd
})

test('Deny hijack of existing domain', async (t) => {
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([{
    modified: fixture.modified,
    domain: fixture.domain,
    ownerId: 'someone-else'
  }])

  const response = await deploy(fixture)
  t.is(response.status, HTTP_STATUS_UNAUTHORIZED)
  const payload = await response.json()
  t.is(payload.message, `Unauthorized domain: "${fixture.domain}"`)
})

test('Deny hi-jack of subdomain', async (t) => {
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([{
    modified: fixture.modified,
    domain: fixture.domain,
    ownerId: 'someone-else'
  }])

  const hijack = `hijack.${fixture.domain}`
  const response = await deploy({
    ...fixture,
    domain: hijack
  })
  t.is(response.status, HTTP_STATUS_UNAUTHORIZED)
  const payload = await response.json()
  t.is(payload.message, `Unauthorized subdomain: "${hijack}"`)
})

test('Allow deployment to *.commons.host subdomain', async (t) => {
  await mongo.db.collection('hosts').deleteMany()
  await mongo.db.collection('hosts').insertMany([{
    modified: fixture.modified,
    domain: 'commons.host',
    ownerId: 'someone-else'
  }])

  const target = 'example.commons.host'
  const response = await deploy({
    ...fixture,
    domain: target
  })
  t.is(response.status, HTTP_STATUS_OK)
})

test('Restore acme.sh', async (t) => {
  configuration.acme.acmesh = original.acmesh
})

test('Stop server', (t) => fastify.close())
