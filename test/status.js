const test = require('blue-tape')
const configuration = require('../core.conf.example.js')
const { fetch } = require('./helpers/fetch')

const server = require('..')

let fastify
test('Start server', async (t) => {
  fastify = await server(configuration)
  await fastify.listen(configuration.port, configuration.host)
})

test('Report status and uptime', async (t) => {
  const url = `https://localhost:${configuration.port}/`
  const response = await fetch(url)
  t.is(response.status, 200)
  const json = await response.json()
  t.is(json.status, 'OK')
  t.is(typeof json.uptime, 'number')
})

test('Also respond with status on plain HTTP', async (t) => {
  const url = `http://localhost:${configuration.acme.port}/`
  const response = await fetch(url)
  t.is(response.status, 200)
  const json = await response.json()
  t.is(json.status, 'OK')
  t.is(typeof json.uptime, 'number')
})

test('Stop server', (t) => fastify.close())
